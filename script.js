/*
 * [...]:
 * -----------------------------------------------
 * @author: LUIS ALBERTO NUNES (DUDU)- CAPTALYS DEV FRONT-END
 * @version: 1.0.0, 10/06/2017
 * @linguage: HTML, CSS, JAVASRCIPT
 * -----------------------------------------------
 * Este é o JS de um pequeno projeto para o dia dos namorados
 * fique à vontade para alterar ou compartilhar.

*/

(function(){

	var heart = document.querySelector('.heart-container');
	var parts = document.querySelectorAll('.heart');
	var campo = document.querySelector('#nome');
	var nome = '';
	var fundo = document.querySelector('.fundo');
	var frase= document.querySelector('.frase');
	var frase1= document.querySelector('.frase1');
	var frase2= document.querySelector('.frase2');

	var animar = function animar() {
		nome = campo.value.toString().toLowerCase();
		switch(nome){
			case 'nataline':
				fundo.style.background="#FAAAC6";
				frase.style.display='block';
				Array.prototype.forEach.call(parts, function (part) {
					part.style.WebkitTransition = 'background 0.5s';
					part.style.MozTransition = 'background 0.5s';
					part.style.background = '#FF3F3F';
				});
				heart.className = 'heart-container heart-animate-fast';
				break;
			case 'web':
					fundo.style.background="#C6A5DF";
					frase1.style.display='block';
					Array.prototype.forEach.call(parts, function (part) {
					part.style.WebkitTransition = 'background 0.5s';
					part.style.MozTransition = 'background 0.5s';
					part.style.background = '#9B6FEB';

				});
				heart.className = 'heart-container heart-animate-slow';
				break;
			case 'luis':
				fundo.style.background="#A0C8E5";
					frase2.style.display='block';
				Array.prototype.forEach.call(parts, function (part) {
					part.style.WebkitTransition = 'background 0.5s';
					part.style.MozTransition = 'background 0.5s';
					part.style.background = '#0292EA';
				});
				heart.className = 'heart-container heart-animate';
				break;
			default:
				Array.prototype.forEach.call(parts, function (part) {
					part.style.WebkitTransition = 'background 0.5s';
					part.style.MozTransition = 'background 0.5s';
					part.style.background = '#CCC';
				});
				heart.className = 'heart-container';
				fundo.style.background="#263238";
				frase.style.display='none';
				frase1.style.display='none';
				frase2.style.display='none';

		}
	}

campo.addEventListener('keyup', animar);
campo.addEventListener('paste', animar);

}());
